import os
import sys
import argparse
from dotenv import load_dotenv
from langchain.chains import LLMChain
from langchain.output_parsers import StructuredOutputParser, ResponseSchema, OutputFixingParser
from langchain.prompts import PromptTemplate, ChatPromptTemplate, HumanMessagePromptTemplate
from langchain.chat_models import ChatOpenAI

load_dotenv()

OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")
if not OPENAI_API_KEY:
    raise ValueError("OPENAI_API_KEY not found in environment variables")

def get_build_output(build_output_file):
    with open(build_output_file, "r") as f:
        return f.read()

def get_chat_instance(api_key):
    return ChatOpenAI(temperature=0, openai_api_key=api_key)

def create_prompt(template, input_vars, partial_vars=None):
    if partial_vars is None:
        partial_vars = {}
    return ChatPromptTemplate.from_messages([
        HumanMessagePromptTemplate(
            prompt=PromptTemplate(
                template=template,
                input_variables=input_vars,
                partial_variables=partial_vars
            )
        )
    ])

def run_chain(chat, prompt, data):
    return LLMChain(llm=chat, prompt=create_prompt(*prompt)).run(data)

def find_filename(chat, build_output):
    return run_chain(
        chat,
        (
            "Can you find the filename where this error comes from: {error}? If you do, please reply with the path to the file ONLY, if not please reply with no.",
            ["error"]
        ),
        build_output
    )

def get_file_contents(filename):
    with open(filename, "r") as f:
        return f.read()

def fix_code(chat, file_text, build_output):
    response_schemas = [
        ResponseSchema(
            name="fix_found",
            description="boolean true false value if the fix was found or not."
        ),
        ResponseSchema(
            name="fixed_content",
            description="the updated contents containing the fix."
        )
    ]
    output_parser = StructuredOutputParser.from_response_schemas(response_schemas)

    return run_chain(
        chat,
        (
            "\nPlease respond with the fixed code ONLY and no additional information. \n{format_instructions}.\n Content: {file}\n Error: {error}.",
            ["file", "error"],
            {"format_instructions": output_parser.get_format_instructions()}
        ),
        {'file': file_text, 'error': build_output}
    ), output_parser
    
def parse_fixed_code(output_parser, fixed_code):
    try:
        return output_parser.parse(fixed_code)
    except:
        return OutputFixingParser.from_llm(parser=output_parser, llm=chat).parse(fixed_code)

def update_file(filename, fixed_content):
    with open(filename, "w") as f:
        f.write(fixed_content)

def main(build_output_file):
    build_output = get_build_output(build_output_file)
    print(build_output)

    chat = get_chat_instance(OPENAI_API_KEY)

    filename = find_filename(chat, build_output)

    if filename == "no":
        print("No filename found")
        return

    print(f"Filename found: {filename}")

    file_text = get_file_contents(filename)

    fixed_code, output_parser = fix_code(chat, file_text, build_output)

    print(fixed_code)

    parsed = parse_fixed_code(output_parser, fixed_code)
    print(parsed)

    if parsed["fix_found"] == "false":
        print("No fix found")
        return

    print(f"Fix found: {parsed['fixed_content']}")

    update_file(filename, parsed["fixed_content"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Automatically fix code errors using LangChain and OpenAI's GPT.")
    parser.add_argument("build_output_file", help="Path to the build output file containing the error message.")
    args = parser.parse_args()
    main(args.build_output_file)